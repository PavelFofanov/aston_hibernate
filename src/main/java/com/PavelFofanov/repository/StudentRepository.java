package com.PavelFofanov.repository;

import com.PavelFofanov.models.Student;

import java.util.List;

public interface StudentRepository {

    void save(Student student);

    void update (Student student, Long id);

    List<Student> findAll();

    Student findByID(Long id);

    void delete(Long id);
}
