package com.PavelFofanov.repository.impl;

import com.PavelFofanov.models.Student;
import com.PavelFofanov.repository.StudentRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityTransaction;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor

public class StudentRepositoryImpl implements StudentRepository {

    private final EntityManager entityManager;

    @Override
    public void save(Student student) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(student);
        transaction.commit();
    }

    @Override
    public void update(Student student, Long id) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        Student studentForUpdate = entityManager.find(Student.class, id);
        if (student.getAge() == null) {
            studentForUpdate.setAge(studentForUpdate.getAge());
        } else {
            studentForUpdate.setAge(student.getAge());
        }
        studentForUpdate.setFirstName(student.getFirstName());
        studentForUpdate.setLastName(student.getLastName());
        transaction.commit();

    }

    @Override
    public List<Student> findAll() {
        return entityManager.createQuery("select student from Student student", Student.class).getResultList();
    }

    @Override
    public Student findByID(Long id) {
        return entityManager.find(Student.class, id);
    }

    @Override
    public void delete(Long id) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        Student studentForDelete = entityManager.find(Student.class, id);
        entityManager.remove(studentForDelete);
        transaction.commit();
    }
}
