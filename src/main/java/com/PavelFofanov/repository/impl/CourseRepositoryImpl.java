package com.PavelFofanov.repository.impl;

import com.PavelFofanov.models.Course;
import com.PavelFofanov.repository.CourseRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityTransaction;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class CourseRepositoryImpl implements CourseRepository {
    private final EntityManager entityManager;

    @Override
    public void save(Course course) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(course);
        transaction.commit();
    }

    @Override
    public void update(Course course, Long id) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        Course courseForUpdate = findByID(id);

        if (course.getDescription() == null) {
            courseForUpdate.setDescription(courseForUpdate.getDescription());
        } else {
            courseForUpdate.setDescription(course.getDescription());
        }

        if (course.getTitle() == null) {
            courseForUpdate.setTitle(courseForUpdate.getTitle());
        } else {
            courseForUpdate.setTitle(course.getTitle());
        }

        if (course.getStart() == null) {
            courseForUpdate.setStart(courseForUpdate.getStart());
        } else {
            courseForUpdate.setStart(course.getStart());
        }

        if (course.getFinish() == null) {
            courseForUpdate.setFinish(courseForUpdate.getFinish());
        } else {
            courseForUpdate.setFinish(course.getFinish());
        }
    }

    @Override
    public List<Course> findAll() {
        return entityManager.createQuery("select course from Course course", Course.class).getResultList();
    }

    @Override
    public Course findByID(Long id) {
        return entityManager.find(Course.class, id);
    }

    @Override
    public void delete(Long id) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        Course courseForDelete = findByID(id);
        entityManager.remove(courseForDelete);
        transaction.commit();
    }
}
