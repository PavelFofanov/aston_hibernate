package com.PavelFofanov.repository;

import com.PavelFofanov.models.Course;
import com.PavelFofanov.models.Student;

import java.util.List;

public interface CourseRepository {

    void save(Course course);

    void update (Course course, Long id);

    List<Course> findAll();

    Course findByID(Long id);

    void delete(Long id);
}
