package com.PavelFofanov;

import com.PavelFofanov.models.Course;
import com.PavelFofanov.models.Student;
import com.PavelFofanov.repository.CourseRepository;
import com.PavelFofanov.repository.StudentRepository;
import com.PavelFofanov.repository.impl.CourseRepositoryImpl;
import com.PavelFofanov.repository.impl.StudentRepositoryImpl;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityTransaction;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.time.LocalDate;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate\\hibernate.cfg.xml");

        SessionFactory sessionFactory = configuration.buildSessionFactory();

        EntityManager entityManager = sessionFactory.createEntityManager();

        StudentRepository studentRepository = new StudentRepositoryImpl(entityManager);
        CourseRepository courseRepository = new CourseRepositoryImpl(entityManager);

        Student student1 = Student.builder()
                .age(33)
                .password("Qwerty001")
                .lastName("Fofanov")
                .firstName("Pavel")
                .email("pf@gmail.com")
                .build();

        Student student2 = Student.builder()
                .age(13)
                .password("Qwerty002")
                .lastName("Petrov")
                .firstName("Igor")
                .email("ip@gmail.com")
                .build();

        Student student3 = Student.builder()
                .age(45)
                .password("Qwerty003")
                .lastName("Krylov")
                .firstName("Yra")
                .email("yk@gmail.com")
                .build();

        Student student4 = Student.builder()
                .age(19)
                .lastName("Pavlov")
                .firstName("Denis")
                .build();

        studentRepository.save(student3);

        studentRepository.delete(1L);

        List<Student> students = studentRepository.findAll();
        for (Student student : students) {
            System.out.println(student);
        }

        studentRepository.update(student4, 102L);

        Course course1 = Course.builder()
                .title("Java")
                .description("Hibernate and JPA")
                .start(LocalDate.of(2023, 1, 1))
                .finish(LocalDate.of(2023, 12, 1))
                .build();
        Course course2 = Course.builder()
                .title("Python")
                .description("Something about Python")
                .start(LocalDate.of(2022, 1, 1))
                .finish(LocalDate.of(2022, 12, 1))
                .build();
        Course course3 = Course.builder()
                .title("Php")
                .description("Something about Php")
                .start(LocalDate.of(2019, 1, 1))
                .finish(LocalDate.of(2020, 12, 1))
                .build();

        courseRepository.save(course2);
        courseRepository.save(course3);

        List<Course> courses = courseRepository.findAll();
        for (Course course : courses) {
            System.out.println(course);
        }

        Course course4 = Course.builder()
                .start(LocalDate.of(2024, 1, 1))
                .finish(LocalDate.of(2025, 12, 1))
                .build();

        courseRepository.update(course4, 2L);

        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        Course course = courseRepository.findByID(2L);
        Student student = studentRepository.findByID(152L);
        student.getCourses().add(course);
        transaction.commit();
        sessionFactory.close();
    }
}
